use std::fmt;
use std::fmt::Display;
use std::ops::*;

#[cfg(test)]
mod tests;

#[repr(C)]
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Complex {
    pub real: f64,
    pub imag: f64,
}

#[link(name = "m")]
extern "C" {
    fn ctan(c: Complex) -> Complex;
    fn csin(c: Complex) -> Complex;
    fn ccos(c: Complex) -> Complex;
    fn conj(c: Complex) -> Complex;
    fn csqrt(c: Complex) -> Complex;
    fn cabs(c: Complex) -> f64;
}

impl Complex {
    pub fn new(real: f64, cplx: f64) -> Complex {
        Complex {
            real: real,
            imag: cplx,
        }
    }
    pub fn tan(self) -> Complex {
        unsafe { ctan(self) }
    }
    pub fn sin(self) -> Complex {
        unsafe { csin(self) }
    }
    pub fn cos(self) -> Complex {
        unsafe { ccos(self) }
    }
    pub fn conj(self) -> Complex {
        unsafe { conj(self) }
    }
    pub fn sqrt(self) -> Complex {
        unsafe { csqrt(self) }
    }
    pub fn abs(self) -> f64 {
        unsafe { cabs(self) }
    }
}

impl From<f64> for Complex {
    fn from(x: f64) -> Complex {
        Complex::new(x, 0.0)
    }
}

impl From<i64> for Complex {
    fn from(x: i64) -> Complex {
        Complex::from(x as f64)
    }
}

impl From<(i64, i64)> for Complex {
    fn from(x: (i64, i64)) -> Complex {
        Complex::new(x.0 as f64, x.1 as f64)
    }
}

impl From<(f64, f64)> for Complex {
    fn from(p: (f64, f64)) -> Complex {
        Complex::new(p.0, p.1)
    }
}

impl Add for Complex {
    type Output = Complex;
    fn add(self, o: Complex) -> Complex {
        Complex::new(self.real + o.real, self.imag + o.imag)
    }
}

impl Sub for Complex {
    type Output = Complex;
    fn sub(self, o: Complex) -> Complex {
        Complex::new(self.real - o.real, self.imag - o.imag)
    }
}

impl Mul for Complex {
    type Output = Complex;
    fn mul(self, o: Complex) -> Complex {
        Complex::new(
            self.real * o.real - self.imag * o.imag,
            self.imag * o.real + self.real * o.imag,
        )
    }
}

impl Div for Complex {
    type Output = Complex;
    fn div(self, o: Complex) -> Complex {
        let a = self * o.conj();
        let z = (o * o.conj()).real;
        Complex::new(a.real / z, a.imag / z)
    }
}

impl Display for Complex {
    fn fmt(&self, w: &mut fmt::Formatter) -> fmt::Result {
        write!(w, "{:?}", self)
    }
}
