use Complex;

#[test]
fn test_abs() {
    assert_eq!(
        Complex::new(10.0, 20.0).abs(),
        Complex::new(20.0, 10.0).abs()
    );
}

#[test]
fn test_add() {
    assert_eq!(
        Complex::new(10.0, 20.0) + Complex::new(20.0, 10.0),
        Complex::new(30.0, 30.0)
    );
}

#[test]
fn test_sub() {
    assert_eq!(
        Complex::new(10.0, 20.0) - Complex::new(20.0, 10.0),
        Complex::new(-10.0, 10.0)
    );
    let a = Complex::new(10.0, 20.0);
    assert_eq!(a - a, Complex::from(0.0));
}

#[test]
fn test_mul() {
    let a = Complex::new(10.0, 20.0);
    let b = Complex::new(20.0, 40.0);
    let c = Complex::from(2);
    assert_eq!((a * c).abs(), a.abs() * c.abs());
    assert_eq!((a * c).abs(), b.abs());
    assert_eq!(a * c, b);
}

#[test]
fn test_div() {
    let a = Complex::new(10.0, 20.0);
    let b = Complex::new(5.0, 10.0);
    let c = Complex::from(2.0);
    assert_eq!((a / c).abs(), a.abs() / 2.0);
    assert_eq!(a / c, b);
}
